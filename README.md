<img src=".asset/banner.png" align=center height=300px>
<br><br>

<div align='center'>
	<h2>🦊 Welcome!</h2><br>
</div>

Hi there. This is my config for Firefox, made entirely with css.<br>
If you like what you see and want to give a try, go to [Installation](#Install) section.<br>
This will only modify the appearance of firefox, nothing more.

* 🪶 Working on version: 99

`ALERT: some things may not work well in the flatpak version`

<br><br>

---

<!--
### 📰 Updates
*work in progress*..
<br><br>
-->

## 📖 Table of content
<!--+ [Q&A](wiki)-->
+ [Screenshots](#Screenshots)
+ [Features](#Features)
+ [Install](#Install)
+ [Uninstall](#Uninstall)
+ [Known Issues](#Issues)

<br><br>

<!-- SCREENSHOTS -->
<div align='center'>
	<h2>🖼️ Screenshots</h2><br>
</div>

### Minla theme
![Preview Minla 1](https://i.imgur.com/jiRd0YY.png)
![Preview Minla 2](https://i.imgur.com/1FRR79b.png)

<br><br>

<!-- FEATURES -->
<div align='center'>
	<h2>💫 General Features</h2><br>
</div>

* Expanded Side-Panel on hover
* Expandible bookmarks bar on hover
* Bigger context menu
* Centered and flat url bar

<br><br>

<!-- INSTALL SECCTION -->
<div align='center'>
	<h2>🚀 Install</h2><br>
</div>

#### 1. Enable CSS

Since Firefox-68 is necessary manually enable the CSS modifications.<br>
To do so, go to: `about:config`<br>
and set `toolkit.legacyUserProfileCustomizations.stylesheets` to true<br>
and `svg.context-properties.content.enabled` to true (optional for treetabstyle)

<br>

#### 2. Setting Up
###### 2.1 Terminal way

Open a terminal and execute:
```bash
	$ cd ~/.mozilla/firefox/*.default-release/
	$ git clone --depth=1 https://gitlab.com/yowls/firefoxcss chrome
```
<br>

###### 2.2 Graphical way

You can download the repository and move the folder to the destination.<br>
To do so, locate the directory going to: `about:profiles`<br>
Look for the profile you´re currently using and click on `open directory` of "*Root Directory*" section.<br>
Then, put the files in a folder called `chrome` (if the folder doenst exist, create it)

<br><br>

#### 3. Additional config

+ If you want to use **Tree Style Tab**:<br>
-> [install addon](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/) <br>
Import my config to fit the look<br>
Copy and paste all [this text](https://gitlab.com/yowls/firefoxcss/-/blob/main/Minla/AditionalConfig/TreeStyleTab.css) in: <br>
`about:addons` > Tree Style Tab > Preferences > Advanced > text box at the end <br>
And set `Appearance > Theme > No decoration`

+ Enable **Title bar** in customize options<br>
for remove visually the whole tab bar <br>
(btw, is where you drag items into toolbar)

<br><br>

<!-- UNINSTALL -->
<div align='center'>
	<h2>🔥 Uninstall</h2><br>
</div>

To **remove/uninstall**, just remove the chrome folder under <br>
your profile directory and everything will return to normal.<br>
Besides to unset the previous firefox config
but it doesn't really matter if you leave it that way<br>

```bash
	$ cd ~/.mozilla/firefox/*.default-release/
	$ rm -rf chrome
```
