#!/bin/bash

# Define colors
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"

FIREFOX_DIR="${HOME}/.mozilla/firefox"
PROFILE="$FIREFOX_DIR/*.default-release"

# Backup previous config
function backup() {
	if [ -d "$PROFILE/chrome" ];then
		echo -e "$colorRed[WARNING]$colorEnd Detecting existent css config"
		echo -e "$colorBlue*Backup$colorEnd previous default-release profile?"

		while [[ "$backup_opt" != "y" && "$backup_opt" != "Y" && "$backup_opt" != "n" ]]
		do
			echo -e -n "Proceed? $colorYellow(Y/n)$colorEnd: "
			read -r backup_opt

			if [ $backup_opt = "y" ] || [ $backup_opt = "Y" ]; then
				mv $PROFILE/chrome $PROFILE/chrome-old
				echo -e "Backup $colorGreen[done]$colorEnd"
			elif [ $backup_opt = "n" ]; then
				echo -e "$colorRed* Directory conflict. Aborting$colorEnd"
				exit 1
			fi
		done
	fi
}


# Implement new config
function implement(){
	echo -e "Clone with: \n\t(1) HTTPs $colorGray(default)$colorEnd \n\t(2) SSH"
	read -r -p "Option: " clone_opt
	if [ $clone_opt -eq 2 ]; then
		echo "Cloning with$colorGreen ssh$colorEnd into $PROFILE/chrome .."
		git clone git@gitlab.com:yowls/qtwm.git ~/.config/qtile
	else
		echo "Cloning with$colorGreen https$colorEnd into $PROFILE/chrome .."
		git clone --depth=1 https://github.com/yowls/firefoxcss $PROFILE/chrome
	fi
}


# Clean up git files
function clean() {
	echo -e "* Remove git files? $colorYelow(y/N)$colorEnd"
	read -r opt_rm

	if [ $opt_rm = "y" ]; then
		rm -rf $PROFILE/chrome/{.git,assets,LICENSE,.gitignore}
		echo "Git files removed"
	fi
}


function main() {
	backup
	implement
	clean
}
