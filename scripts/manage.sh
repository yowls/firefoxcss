#/bin/sh

# VARIABLES
OPTION=${1:-none}
HERE=$(pwd)

# DEFINE COLORS
colorRed="\033[0;31m"
colorYellow="\033[0;33m"
colorGreen="\033[0;32m"
colorBlue="\033[0;34m"
colorGray="\033[0;37m"
colorEnd="\033[0m"

# DEFINE MESSAGES
debug_info() {
	echo -e "$colorBlue[INFO]$colorEnd $1"
}
debug_alert() {
	echo -e "$colorYellow[ALERT]$colorEnd $1"
}
debug_error() {
	echo -e "$colorRed[ERROR]$colorEnd $1"
}

# USER PROFILE
FIREFOX_DIR="$HOME/.mozilla/firefox"

# Detect user profile
# TODO: check what happen if there are multiples *.default-profile
[ -d ~/.mozilla/firefox/*.default-profile/ ] && PROFILE_DIR=~/.mozilla/firefox/*.default-profile/ || PROFILE_DIR=~/.mozilla/firefox/*.default
CHROME_DIR=$PROFILE_DIR/chrome


# TODO: Add option to check which theme is applied (if exist)
show_help() {
	cat << EOF
Manage firefoxcss themes
Usage: $0 [apply | clean | help]

	apply           | Create symlink of the theme in the profile
	clean           | Remove symlink
	list            | List themes
	help            | Show this message
EOF
}

# FIXME: dont work well
clean_symlink() {
	# Check if chrome dir exists and is a symblink
	if [[ -L $CHROME_DIR && -e $CHROME_DIR ]]; then
		debug_info "Target: $CHROME_DIR"
		#rm $CHROME_DIR
		debug_info "Removed"
	else
		debug_error "Backup your '$CHROME_DIR' first"
		exit 1
	fi
}

list_themes() {
	echo THEMES:
	echo themes/*
}

if [ $OPTION = "apply" ]; then
	THEME=${2:-none}

	if [ $THEME = "none" ]; then
		debug_error "Parameter {theme} requiered"
		list_themes
		exit 1
	fi

	# TODO: check errors if THEME is not set correclty
	clean_symlink

	debug_info "Using profile:"
	echo $PROFILE_DIR/
	debug_info "Theme: $THEME"

	debug_info "Applying.."
	# ln -s $HERE/themes/$THEME $PROFILE_DIR/chrome
	debug_info "Done"

elif [ $OPTION = "clean" ]; then
	debug_info "Cleaning.."
	clean_symlink

elif [ $OPTION = "list" ]; then
	list_themes

elif [ $OPTION = "help" ] || [ $OPTION = "-h" ] || [ $OPTION = "--help" ]; then
	show_help

else
	debug_error "Parameter expected \n"
	show_help
fi
